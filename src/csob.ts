import {main, MessagePartGetter, Row} from "./main.js"
import {FetchMessageObject} from "imapflow";
import {Buffer} from "buffer";
import {execute} from "./execute.js";
import SaxonJS from "saxon-js";

type CSOBRow = {
	date: string,
	accountingDate: string,
	dueDate: string,
	account: string,
	card: string,
	kind: string,
	counterAccount: string,
	counterName: string,
	amount: number,
	variableSymbol: number,
	constantSymbol: number,
	specificSymbol: number,
	balance: number,
	availableBalance: number,
	info: string,
	subject: string,
	messageDatetime: string,
}

// Xmllint is used to substitute entity values for entity references (i.e. replace &nbsp; with &#xA0;)
async function lint(html: Buffer): Promise<Buffer> {
	const doctype = Buffer.from(`<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" [
		<!ENTITY % w3centities-f PUBLIC "-//W3C//ENTITIES Combined Set//EN//XML" "http://www.w3.org/2003/entities/2007/w3centities-f.ent"> %w3centities-f;
	]>`)
	return await execute(
		'xmllint',
		['--format', '--noent', '-'],
		Buffer.concat([doctype, html])
	)
}

async function transform(xml: Buffer): Promise<string> {
	// See https://www.saxonica.com/saxon-js/documentation2/index.html#!api/transform
	const {principalResult: json} = SaxonJS.transform({
		stylesheetFileName: "csob/csob.sef.json",
		sourceText: xml.toString(),
		destination: "serialized"
	})
	return json
}

async function getter(message: FetchMessageObject, messagePartGetter: MessagePartGetter): Promise<Row[]> {
	const {content: html} = await messagePartGetter('1')
	const xml = await lint(html)
	const json = await transform(xml)
	const sparseRows: CSOBRow[] = JSON.parse(json)
	const emptyRow: CSOBRow = {
		date: null,
		accountingDate: null,
		dueDate: null,
		account: null,
		card: null,
		kind: null,
		counterAccount: null,
		counterName: null,
		amount: null,
		variableSymbol: null,
		constantSymbol: null,
		specificSymbol: null,
		balance: null,
		availableBalance: null,
		info: null,
		subject: message.envelope.subject,
		messageDatetime: message.envelope.date.toLocaleString('cs-CZ').replace(/\. /g, '.'),
	}

	return sparseRows.map(sparseRow => {
		return {...emptyRow, ...sparseRow}
	})
}

await main(getter, null, {
	seen: false,
	or: [
		{subject: 'Moje info - Avízo'},
		{subject: 'Moje info - Zůstatek na účtu'},
		{subject: 'Moje info - Platba kartou'},
		{subject: 'Moje info - Potvrzení přijetí platebního příkazu'},
	]
})
