import {google} from 'googleapis'
import config from './config.js'

export const sheets = google.sheets({
	version: "v4",
	auth: new google.auth.GoogleAuth({
		keyFile: config.keyFile,
		scopes: 'https://www.googleapis.com/auth/spreadsheets',
	})
})
