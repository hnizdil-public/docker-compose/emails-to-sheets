import {drive} from "./drive.js"
import config from "./config.js"
import {promises as fs} from 'fs'
import * as path from "path"
import * as os from "os"
import {Readable} from "stream"
import {main, MessagePartGetter, Row} from "./main.js"
import {FetchMessageObject} from "imapflow";
import {execute} from "./execute.js";

type ExtractResult = { pdfBuffer: Buffer, pdfFileName: string }

type PayslipRow = {
	obdobi: string,
	tarif: number | undefined,
	inflaceMesicniProcento: number | undefined,
	inflaceMesicniIndex: number | undefined,
	inflaceDelta: number | undefined,
	tarifDelta: number | undefined,
	plat: number,
	hrubaMzda: number,
	cistyPrijem: number,
	homeOffice: number,
	naUcet: number,
	tydenniUvazek: number,
	hodiny: number,
	dny: number,
	dovolenaZbyv: number,
	dovolenaPrumer: number,
	dovolenaHodiny: number | undefined,
	dovolenaDny: number | undefined,
	dovolenaMzda: number | undefined,
	neplaceneVolnoHodiny: number | undefined,
	neplaceneVolnoDny: number | undefined,
	neplaceneVolnoMzda: number | undefined,
	nemocPrumer: number,
	nemocHodiny: number | undefined,
	nemocDny: number | undefined,
	nemocDavky: number | undefined,
	premieSluzba: number | undefined,
	premieOnCall: number | undefined,
	odmenaMimoradna: number | undefined,
	odmenaQ: number | undefined,
	odmenaRocni: number | undefined,
	benefitPlus: number | undefined,
	danVypoctena: number | undefined,
	danZakladni: number | undefined,
	danDeti: number | undefined,
	danZaloha: number | undefined,
	danZalohaSolidarni: number | undefined,
	pojisteniZdravotni: number,
	pojisteniSocialni: number,
	pojisteniZdravotniZam: number,
	pojisteniSocialniZam: number,
	zdroj: string,
	emailDatum: string,
}

const identsToColumns: { [key: number]: keyof PayslipRow } = {
	3111: "premieSluzba",
	3112: "premieOnCall",
	3322: "odmenaQ",
	3561: "odmenaMimoradna",
	3411: "odmenaRocni",
	7111: "danZaloha",
	7201: "pojisteniZdravotni",
	7202: "pojisteniSocialni",
	5591: "homeOffice",
	9051: "benefitPlus",
	9401: "pojisteniZdravotniZam",
	9402: "pojisteniSocialniZam",
	9411: "danZalohaSolidarni",
	7411: "naUcet",
}

const textsToColumns: { [key: string]: keyof PayslipRow } = {
	"HRUBÁ MZDA": "hrubaMzda",
	"VYPOČTENÁ ZÁLOHA NA DAŇ": "danVypoctena",
	"Sleva na dani - ZÁKLADNÍ": "danZakladni",
	"Daňové zvýhodnění - DĚTI": "danDeti",
	"ČISTÝ PŘÍJEM": "cistyPrijem",
}

function changeExtension(file: string, extension: string): string {
	const basename = path.basename(file, path.extname(file))
	return path.join(path.dirname(file), basename + extension)
}

async function getAttachment(
	message: FetchMessageObject,
	messagePartGetter: MessagePartGetter
): Promise<ExtractResult> {
	for (const part of message.bodyStructure.childNodes) {
		if (part.disposition === 'attachment') {
			// Set chunkSize big enough. Don't know why, but only one chunk would get read,
			// no matter the attachment size.
			const {meta, content} = await messagePartGetter(part.part, {chunkSize: 500000})
			return extract(content, meta.filename)
		}
	}
}

async function saveToDrive(pdfBuffer: Buffer, filename: string): Promise<void> {
	drive.files.create({
		requestBody: {
			name: filename,
			parents: [config.folderId]
		},
		media: {
			body: Readable.from(pdfBuffer)
		}
	})
}

// 7z is required. Regular unzip fails with "unsupported compression method 99".
async function extract(zipBuffer: Buffer, zipFileName: string): Promise<ExtractResult> {
	const workdirPath = await fs.mkdtemp(path.join(os.tmpdir(), 'payslip-zip-'))
	const zipFilePath = path.join(workdirPath, zipFileName)
	const pdfFileName = changeExtension(zipFileName, '.pdf')
	await fs.writeFile(zipFilePath, zipBuffer)

	try {
		const pdfBuffer = await execute('7z', ['e', `-p${config.zipPassword}`, '-so', zipFilePath])
		return {pdfBuffer, pdfFileName}
	}
	finally {
		await fs.rm(zipFilePath)
		await fs.rmdir(workdirPath)
	}
}

async function pdfToText(pdfBuffer: Buffer): Promise<string> {
	const text= await execute('pdftotext', ['-layout', '-', '-'], pdfBuffer)
	return text.toString()
}

function parseBody(body: string): PayslipRow {
	const row: PayslipRow = {
		obdobi: "",
		tarif: null,
		inflaceMesicniProcento: null,
		inflaceMesicniIndex: null,
		inflaceDelta: null,
		tarifDelta: null,
		plat: 0,
		hrubaMzda: 0,
		cistyPrijem: 0,
		homeOffice: 0,
		naUcet: 0,
		tydenniUvazek: 0,
		hodiny: 0,
		dny: 0,
		dovolenaZbyv: 0,
		dovolenaPrumer: 0,
		dovolenaHodiny: null,
		dovolenaDny: null,
		dovolenaMzda: null,
		neplaceneVolnoHodiny: null,
		neplaceneVolnoDny: null,
		neplaceneVolnoMzda: null,
		nemocPrumer: 0,
		nemocHodiny: null,
		nemocDny: null,
		nemocDavky: null,
		premieSluzba: null,
		premieOnCall: null,
		odmenaMimoradna: null,
		odmenaQ: null,
		odmenaRocni: null,
		benefitPlus: null,
		danVypoctena: null,
		danZakladni: null,
		danDeti: null,
		danZaloha: null,
		danZalohaSolidarni: null,
		pojisteniZdravotni: 0,
		pojisteniSocialni: 0,
		pojisteniZdravotniZam: 0,
		pojisteniSocialniZam: 0,
		zdroj: "",
		emailDatum: "",
	}
	let tarifFound = false

	for (const line of body.split('\n')) {
		const lastColumn = line.substring(125).trim()
		if (lastColumn.match(/\d{1,2}\/\d{4}/)) {
			row.obdobi = lastColumn
			continue
		}

		const amount = parseInt(lastColumn.replace(/ /g, ''))
		if (isNaN(amount)) {
			continue
		}
		if (amount === 0 && !tarifFound) {
			row.tarif = parseInt(line.substring(0, 20).trim())
			row.tydenniUvazek = parseFloat(line.substring(20, 45).trim())
			row.dovolenaZbyv = parseFloat(line.substring(45, 70).trim())
			row.dovolenaPrumer = parseFloat(line.substring(70, 95).trim().replace(/ .*/, ''))
			row.nemocPrumer = parseFloat(line.substring(95, 125).trim().replace(/ .*/, ''))
			tarifFound = true
			continue
		}

		const ident = parseInt(line.substring(0, 14).trim())
		const text = line.substring(14, 120).trim()

		let column = identsToColumns[ident] ?? textsToColumns[text]
		if (column) {
			(row[column] as number) = amount
			continue
		}

		const hours = parseFloat(line.substring(75, 95).trim())
		const days = parseFloat(line.substring(95, 125).trim())

		// https://stackoverflow.com/questions/67642380/how-to-declare-indexer-type-of-function-argument-type-that-tk-is-of-type-x-so#comment119562161_67642380
		const setAmountHoursDays = <T extends { [key in K]: number }, K extends keyof T>(
			row: T,
			amountCol: K & keyof T,
			hoursCol: K & keyof T,
			daysCol: K & keyof T,
		) => {
			const _row: { [key in K]: number } = row
			_row[amountCol] ??= 0
			_row[amountCol] += amount
			if (!isNaN(hours)) {
				_row[hoursCol] ??= 0
				_row[hoursCol] += hours
			}
			if (!isNaN(days)) {
				_row[daysCol] ??= 0
				_row[daysCol] += days
			}
		}

		switch (ident) {
			case 1131:
				setAmountHoursDays(row, "plat", "hodiny", "dny")
				break
			case 6619:
				setAmountHoursDays(row, "nemocDavky", "nemocHodiny", "nemocDny")
				break
			case 6510:
			case 6511:
				setAmountHoursDays(row, "dovolenaMzda", "dovolenaHodiny", "dovolenaDny")
				break
			case 810:
				setAmountHoursDays(row, "neplaceneVolnoMzda", "neplaceneVolnoHodiny", "neplaceneVolnoDny")
				break
		}
	}

	return row
}

async function getter(message: FetchMessageObject, messagePartGetter: MessagePartGetter): Promise<Row[]> {
	const {pdfBuffer, pdfFileName} = await getAttachment(message, messagePartGetter)
	await saveToDrive(pdfBuffer, pdfFileName)
	let body = await pdfToText(pdfBuffer)
	const date = new Date(message.internalDate)
	const dateForSheets = `${date.getDate()}.${date.getMonth() + 1}.${date.getFullYear()}`
		+ ` ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`

	const row = parseBody(body)
	row.zdroj = pdfFileName
	row.emailDatum = dateForSheets

	return [row]
}

await main(getter)
