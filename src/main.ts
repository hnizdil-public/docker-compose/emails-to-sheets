import {createMimeMessage} from 'mimetext'
import config from "./config.js"
import {sheets} from "./sheets.js"
import {FetchMessageObject, FetchQueryObject, ImapFlow, SearchObject} from "imapflow"
import {Thread} from "./thread.js";

export type Row = { [key: string]: string | number | undefined }
export type RowGetter = (message: FetchMessageObject, messagePartGetter: MessagePartGetter) => Promise<Row[]>
export type RowsSorter = (rows: Row[]) => Row[]
export type MessagePartGetter = (part: string, options?: DownloadOptions) => Promise<MessagePart>

type MessagePart = { meta: { filename?: string }, content: Buffer }
type DownloadOptions = { uid?: boolean; maxBytes?: number, chunkSize?: number }
type AppendResult = {
	thread: Thread,
	result: {
		data: {
			updates?: {
				updatedRange?: string
			}
		}
	}
}

async function fetchMessages(imap: ImapFlow, search?: SearchObject): Promise<FetchMessageObject[]> {
	let query: FetchQueryObject = {
		envelope: true,
		threadId: "true", // docs say bool, but it is a string
		internalDate: true,
		bodyStructure: true,
	}
	let messages: FetchMessageObject[] = []
	for await (const message of imap.fetch(search ?? {seen: false}, query)) {
		messages.push(message)
	}
	return messages.sort((a, b) => a.envelope.date.getTime() - b.envelope.date.getTime())
}

async function getMessagePart(
	imap: ImapFlow,
	message: FetchMessageObject,
	part: string,
	options?: DownloadOptions
): Promise<MessagePart> {
	const {meta, content} = await imap.download(`${message.seq}`, part, options)
	const chunks = []
	for await (let chunk of content) {
		chunks.push(chunk)
	}
	return {meta, content: Buffer.concat(chunks)}
}

async function getThreads(
	imap: ImapFlow,
	messages: FetchMessageObject[],
	rowGetter: RowGetter,
	sorter: RowsSorter
): Promise<Thread[]> {
	const messagesByThreadId: { [key: string]: FetchMessageObject[] } = {}
	for (const message of messages) {
		messagesByThreadId[message.threadId] ??= []
		messagesByThreadId[message.threadId].push(message)
	}

	return Promise.all(
		Object.keys(messagesByThreadId).map(async threadId => {
			const messages = messagesByThreadId[threadId]
			const rows = await Promise.all(
				messages.map(message =>
					rowGetter(message, async (part, options = {}) =>
						getMessagePart(imap, message, part, options)
					)
				)
			)
			return new Thread(threadId, messages, sorter(rows.flat()))
		})
	)
}

async function appendRows(threads: Thread[]): Promise<AppendResult[]> {
	let results: AppendResult[] = []
	for (let thread of threads) {
		const result = await sheets.spreadsheets.values.append({
			spreadsheetId: config.spreadsheetId,
			range: config.spreadsheetRange,
			valueInputOption: 'USER_ENTERED',
			insertDataOption: 'INSERT_ROWS',
			requestBody: {
				majorDimension: 'ROWS',
				range: config.spreadsheetRange,
				values: thread.rows.map((row: Row) => Object.values(row)),
			},
		})
		results.push({thread, result})
	}
	return results
}

async function appendReplies(imap: ImapFlow, appendResults: AppendResult[]): Promise<void> {
	await Promise.all(
		appendResults.map(result => {
			console.table(result.thread.rows)
			return Promise.all([
				imap.append(
					config.mailbox,
					createResponseMessage(result).asRaw(),
					['\\Seen']
				),
				imap.messageFlagsAdd({threadId: result.thread.id}, ['\\Seen'])
			])
		})
	)
}

function createResponseMessage(appendResult: AppendResult) {
	const json = JSON.stringify(appendResult.result.data, null, 2)
	const pre = `<code style="white-space: pre-wrap;">${json}</code>`
	const range = appendResult.result.data.updates.updatedRange.replace(/^.*!/, '')
	const lastMessageId = appendResult.thread.lastMessage.envelope.messageId
	const message = createMimeMessage()
	const link = 'https://docs.google.com/spreadsheets/d/'
		+ `${config.spreadsheetId}/edit#gid=${config.spreadsheetTabId}&range=${range}`

	message.setSender('robot')
	message.setRecipient(config.imapUser)
	message.setHeader('In-Reply-To', lastMessageId)
	message.setHeader('References', lastMessageId)
	message.setSubject(`Re: ${appendResult.thread.lastMessage.envelope.subject}`)
	message.addMessage({contentType: 'text/html', data: `${link}<br />${pre}`})

	return message
}

export async function main(getter: RowGetter, sorter?: RowsSorter, search?: SearchObject) {
	const imap = new ImapFlow({
		host: 'imap.gmail.com',
		port: 993,
		secure: true,
		logger: false,
		disableAutoIdle: true,
		auth: {
			user: config.imapUser,
			pass: config.imapPass,
		}
	})

	imap.on('error', error => {
		console.error(error)
	})

	const noOpSorter = (rows: Row[]): Row[] => rows

	await imap.connect()
	let lock = await imap.getMailboxLock(config.mailbox)
	try {
		const messages = await fetchMessages(imap, search)
		if (!messages) {
			return
		}
		const threads = await getThreads(imap, messages, getter, sorter ?? noOpSorter)
		const results = await appendRows(threads)
		await appendReplies(imap, results)
		await imap.logout()
	} finally {
		lock.release()
	}
}
