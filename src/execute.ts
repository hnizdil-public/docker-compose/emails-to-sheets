import {Buffer} from "buffer";
import config from "./config.js";
import {spawn} from "child_process";

export async function execute(command, args: string[], input?: Buffer): Promise<Buffer> {
	const child = spawn(command, args, {timeout: config.childProcessTimeout})

	if (input) {
		child.stdin.end(input)
	}

	let buffers: Buffer[] = []
	child.stdout.on('data', (chunk) => {
		buffers.push(chunk)
	})

	child.stderr.pipe(process.stderr)

	const status = await new Promise((resolve, reject) => {
		child.on('close', resolve)
		child.on('error', reject)
	})

	if (child.killed) {
		throw new Error(`Command '${command}' was killed`)
	}

	if (status != 0) {
		throw new Error(`Command '${command}' failed with status ${status}`)
	}

	return Buffer.concat(buffers)
}
