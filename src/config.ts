export default {
	keyFile: process.env.KEY_FILE,
	imapUser: process.env.IMAP_USER,
	imapPass: process.env.IMAP_PASS,
	mailbox: process.env.MAILBOX,
	spreadsheetId: process.env.SPREADSHEET_ID,
	spreadsheetTabId: process.env.SPREADSHEET_TAB_ID,
	spreadsheetRange: process.env.SPREADSHEET_RANGE,
	zipPassword: process.env.ZIP_PASSWORD,
	folderId: process.env.FOLDER_ID,
	childProcessTimeout: 10000
}
