// Google Apps Script for deduplicating balance rows. Use with triggers to run periodically.

function main() {
  deduplicate('Trans', 'Subject', 'Moje info - Zůstatek na účtu', ['Balance', 'Avail. Balance'], 'Dedup')
}

function deduplicate(sheetName, targetColName, searchValue, valuesColNames, markColName) {
  var spreadsheet = SpreadsheetApp.getActiveSpreadsheet()
  var sheet = spreadsheet.getSheetByName(sheetName)

  var headerRange = sheet.getRange(1, 1, 1, sheet.getLastColumn())
  var headerValues = headerRange.getValues()[0]

  const mark = 'visited'
  const markColIndex = headerValues.indexOf(markColName) + 1
  const lastMarkedRowIndex = getLastMarkedRowIndex(sheet, markColIndex, mark)

  const targetColValues = getColValues(sheet, headerValues, targetColName, lastMarkedRowIndex)
  const valuesColValues = valuesColNames.map((colName) => {
    return getColValues(sheet, headerValues, colName, lastMarkedRowIndex)
  });

  console.log('Starting from row %d', lastMarkedRowIndex)

  var lastValues = []
  for (var i = targetColValues.length; i >= 0; i--) {
    if (targetColValues[i] != searchValue) {
      lastValues = []
      continue
    }
    values = valuesColValues.map(v => v[i])
    if (lastValues.length == 0) {
      lastValues = values
    } else if (arraysEqual(values, lastValues)) {
      sheet.deleteRow(lastMarkedRowIndex + i)
      console.log('Deleted row %d', lastMarkedRowIndex + i)
    } else {
      lastValues = values
    }
  }

  const lastRowIndex = sheet.getLastRow()
  sheet.getRange(lastRowIndex, markColIndex).setValue(mark)
  console.log('Marked row %d as visited', lastRowIndex)
}

function getColValues(sheet, headerValues, colName, startRowIndex) {
  colIndex = headerValues.indexOf(colName) + 1
  return sheet
    .getRange(startRowIndex, colIndex, sheet.getLastRow())
    .getValues()
    .map(v => v[0])
}

function getLastMarkedRowIndex(sheet, colIndex, mark) {
  const colRange = sheet.getRange(1, colIndex, sheet.getLastRow())
  const finder = colRange.createTextFinder(mark)
  const matches = finder.findAll()
  const lastMatch = matches.pop()

  if (lastMatch) {
    return lastMatch.getRow() + 1
  } else {
    return 2 // skipping the header
  }
}

function arraysEqual(a, b) {
  if (a.length !== b.length) {
    return false
  }

  for (let i = 0; i < a.length; i++) {
    if (a[i] !== b[i]) {
      return false
    }
  }

  return true
}
