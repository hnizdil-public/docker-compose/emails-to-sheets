<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="3.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xhtml="http://www.w3.org/1999/xhtml"
                xmlns="http://www.w3.org/2005/xpath-functions">

    <xsl:output method="text" encoding="UTF-8"/>

    <xsl:template match="xhtml:body">
        <xsl:variable name="xml">
            <array>
                <xsl:apply-templates select="xhtml:table"/>
            </array>
        </xsl:variable>
        <!-- @see https://www.w3.org/TR/xpath-functions-31/#func-xml-to-json -->
        <xsl:value-of select="xml-to-json($xml)"/>
    </xsl:template>

    <xsl:template name="parseAmount">
        <xsl:param name="amount"/>
        <xsl:variable name="withoutNumericalChars" select="translate($amount, '0123456789,-', '')"/>
        <xsl:variable name="withDecimalComma" select="translate($amount, $withoutNumericalChars, '')"/>
        <xsl:value-of select="normalize-space(translate($withDecimalComma, ',', '.'))"/>
    </xsl:template>

    <xsl:template name="entry">
        <xsl:param name="key"/>
        <xsl:param name="content"/>
        <xsl:variable name="node" select="*/xhtml:td[preceding-sibling::xhtml:td[normalize-space()=$content]]"/>
        <xsl:if test="$node">
            <string key="{$key}">
                <xsl:value-of select="normalize-space($node)"/>
            </string>
        </xsl:if>
    </xsl:template>

    <xsl:template name="numberEntry">
        <xsl:param name="key"/>
        <xsl:param name="content"/>
        <xsl:variable name="node" select="*/xhtml:td[preceding-sibling::xhtml:td[normalize-space()=$content]]"/>
        <xsl:if test="$node">
            <number key="{$key}">
                <xsl:call-template name="parseAmount">
                    <xsl:with-param name="amount" select="$node"/>
                </xsl:call-template>
            </number>
        </xsl:if>
    </xsl:template>

    <xsl:template
            match="xhtml:table[preceding-sibling::xhtml:p[
                contains(xhtml:b, 'Detail')
                or contains(xhtml:b, 'Parametry platby')
                or contains(xhtml:b, 'Parametry příkazu')
                or contains(xhtml:b, 'Parametry transakce')
            ]]">
        <map>
            <xsl:variable name="datePattern" select="'dne (\d+\.\d+.\d+) .*'"/>
            <xsl:variable name="dateRowNode" select="
              ancestor::xhtml:tr[1]/
                preceding-sibling::xhtml:tr[matches(., $datePattern)][1]
            "/>
            <xsl:if test="$dateRowNode">
                <string key="date">
                    <xsl:value-of select="replace(normalize-space($dateRowNode), $datePattern, '$1')"/>
                </string>
            </xsl:if>

            <xsl:variable name="kindPattern" select="'.* transakce typu: ([^.]+)\..*'"/>
            <xsl:choose>
                <xsl:when test="$dateRowNode and matches($dateRowNode, $kindPattern)">
                    <string key="kind">
                        <xsl:value-of select="replace(normalize-space($dateRowNode), $kindPattern, '$1')"/>
                    </string>
                </xsl:when>
                <xsl:when test="$dateRowNode and matches($dateRowNode, 'transakce platební kartou')">
                    <string key="kind">
                        <xsl:value-of select="'transakce platební kartou'"/>
                    </string>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:call-template name="entry">
                        <xsl:with-param name="key" select="'kind'"/>
                        <xsl:with-param name="content" select="'Typ příkazu'"/>
                    </xsl:call-template>
                </xsl:otherwise>
            </xsl:choose>

            <xsl:call-template name="entry">
                <xsl:with-param name="key" select="'dueDate'"/>
                <xsl:with-param name="content" select="'Datum splatnosti'"/>
            </xsl:call-template>

            <xsl:call-template name="entry">
                <xsl:with-param name="key" select="'accountingDate'"/>
                <xsl:with-param name="content" select="'Datum účtování'"/>
            </xsl:call-template>

            <xsl:call-template name="entry">
                <xsl:with-param name="key" select="'account'"/>
                <xsl:with-param name="content" select="'Účet'"/>
            </xsl:call-template>
            <xsl:call-template name="entry">
                <xsl:with-param name="key" select="'account'"/>
                <xsl:with-param name="content" select="'Účet ke kartě'"/>
            </xsl:call-template>

            <xsl:call-template name="entry">
                <xsl:with-param name="key" select="'card'"/>
                <xsl:with-param name="content" select="'Číslo karty'"/>
            </xsl:call-template>

            <xsl:call-template name="entry">
                <xsl:with-param name="key" select="'counterAccount'"/>
                <xsl:with-param name="content" select="'Účet protistrany'"/>
            </xsl:call-template>

            <xsl:call-template name="entry">
                <xsl:with-param name="key" select="'counterName'"/>
                <xsl:with-param name="content" select="'Název protistrany'"/>
            </xsl:call-template>

            <xsl:call-template name="entry">
                <xsl:with-param name="key" select="'variableSymbol'"/>
                <xsl:with-param name="content" select="'Variabilní symbol'"/>
            </xsl:call-template>

            <xsl:call-template name="entry">
                <xsl:with-param name="key" select="'constantSymbol'"/>
                <xsl:with-param name="content" select="'Konstantní symbol'"/>
            </xsl:call-template>

            <xsl:call-template name="entry">
                <xsl:with-param name="key" select="'specificSymbol'"/>
                <xsl:with-param name="content" select="'Specifický symbol'"/>
            </xsl:call-template>

            <xsl:call-template name="entry">
                <xsl:with-param name="key" select="'info'"/>
                <xsl:with-param name="content" select="'Zpráva pro příjemce'"/>
            </xsl:call-template>

            <xsl:call-template name="entry">
                <xsl:with-param name="key" select="'info'"/>
                <xsl:with-param name="content" select="'Místo'"/>
            </xsl:call-template>

            <xsl:call-template name="numberEntry">
                <xsl:with-param name="key" select="'amount'"/>
                <xsl:with-param name="content" select="'Částka'"/>
            </xsl:call-template>
            <xsl:call-template name="numberEntry">
                <xsl:with-param name="key" select="'amount'"/>
                <xsl:with-param name="content" select="'Částka blokace'"/>
            </xsl:call-template>

            <xsl:call-template name="numberEntry">
                <xsl:with-param name="key" select="'availableBalance'"/>
                <xsl:with-param name="content" select="'Disponibilní zůstatek'"/>
            </xsl:call-template>

            <xsl:variable name="balanceCellNode"
                          select="*/xhtml:td[preceding-sibling::xhtml:td[normalize-space()='Aktuální zůstatek']]"/>
            <xsl:variable name="balanceRowNode"
                          select="ancestor::xhtml:tr[1]/following-sibling::xhtml:tr[contains(., 'Zůstatek')][1]"/>
            <xsl:choose>
                <xsl:when test="$balanceCellNode">
                    <number key="balance">
                        <xsl:call-template name="parseAmount">
                            <xsl:with-param name="amount" select="$balanceCellNode"/>
                        </xsl:call-template>
                    </number>
                </xsl:when>
                <xsl:when test="$balanceRowNode">
                    <number key="balance">
                        <xsl:call-template name="parseAmount">
                            <xsl:with-param name="amount"
                                            select="replace(normalize-space($balanceRowNode), '.*:\s*', '')"/>
                        </xsl:call-template>
                    </number>
                </xsl:when>
            </xsl:choose>

        </map>
    </xsl:template>

    <!-- Ignore rest of the nodes -->
    <xsl:template match="node()|@*">
        <xsl:apply-templates/>
    </xsl:template>

</xsl:stylesheet>
