# E-mails to Sheets

Fetches unread messages from the specified mailbox (Gmail label) through IMAP.
Then parses email body or attachment for spreadsheet rows and appends those rows to Google Sheet.

## How to run

Can be run with docker-compose.

```shell
docker compose run --rm airbank
docker compose run --rm payslip
docker compose run --rm csob
```

The application will probably be run with cron like so:

```cron
MAILTO="john@doe.com"
*/5 7-23 * * * cd /app && docker compose --progress quiet run --rm airbank >>airbank.log
*/5 7-23 * * * cd /app && docker compose --progress quiet run --rm payslip >>payslip.log
*/5 7-23 * * * cd /app && docker compose --progress quiet run --rm csob    >>csob.log
```

Usually, we want to receive an email when something goes wrong.
Cron sends any command output to the specified email address.
Since stdout gets redirected to a file, anything sent to stderr should trigger an email.

## Air Bank

Parses email body directly.

## ČSOB

Applies XSLT to HTML email body leveraging [xml-to-json][xml-to-json] XPath function.
Resulting JSON is then directly parsed.

Use `npm run sef` to convert XSLT to SEF JSON (stylesheet export file) required by SaxonJS.

[xml-to-json]: https://www.w3.org/TR/xpath-functions-31/#func-xml-to-json

To debug XSL transformation on test data contained in `csob/emails.xml`, run `npm run debug-xslt`.

## Payslip

Extracts PDF from password-protected zip file. Then uses `pdftotext` and parses the resulting text.
