#!/bin/zsh

set -e

function get_secret() {
  readonly key=$1
  (
    cd ../ansible-elvis \
      && source venv/bin/activate \
      && ./vault.sh view roles/emails-to-sheets/vars/vault.yml | yq $key
  )
}

cd ${0:A:h}

get_secret .airbank_env >.airbank.env
get_secret .csob_env >.csob.env
get_secret .payslip_env >.payslip.env
get_secret .service_account_json >.service-account.json
